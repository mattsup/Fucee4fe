FROM nginx:alpine

ADD app /usr/share/nginx/html

EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
